import torch
from peft import (
    LoraConfig, get_peft_model,
    prepare_model_for_int8_training, set_peft_model_state_dict,
    TaskType
)
from transformers import (
    AutoModelForCausalLM, AutoTokenizer,
    BatchEncoding, PreTrainedTokenizer,
    DataCollatorForSeq2Seq, Trainer, TrainingArguments
)
from typing import Dict, Union
import yaml
import pandas as pd
import os
from torch.utils.data import Dataset
from torch import Tensor


with open("config.yaml", mode="r") as file_obj:
    config: Dict = yaml.safe_load(file_obj)


tokenizer: PreTrainedTokenizer = AutoTokenizer.from_pretrained(
    pretrained_model_name_or_path=config["pretrained_model"]
)
tokenizer.pad_token_id = 0
tokenizer.padding_side = "left"


def generate_prompt(row) -> str:
    """
    Generate prompt for language model
    :param row: pandas row
    :return: prompt
    """
    genre: str = row["specific_genre"]
    if genre == "câu đối":
        prompt: str = f"Một {genre}"
    elif genre.startswith("thơ"):
        prompt: str = f"Một bài {genre}"
    else:
        prompt: str = f"Một bài thơ {genre}"

    title: str = row["title"]
    if len(title) > 0:
        prompt += f" có tựa đề '{title}'"

    content: str = row["content"]
    prompt += f"\n\n{content}"

    return prompt


def tokenize_text(text: str):
    """
    Tokenize text
    :param text: text
    :return: dict contains
        - input_ids
        - attention_mask
        - labels
    """
    result: BatchEncoding = tokenizer(
        text=text, truncation=True, max_length=config["max_length"]
    )

    if (
        config["add_eos_token"] and
        len(result["input_ids"]) < config["max_length"] and
        result["input_ids"][-1] != tokenizer.eos_token_id
    ):
        result["input_ids"].append(
            tokenizer.eos_token_id
        )
        result["attention_mask"].append(1.0)

    result["labels"] = result["input_ids"].copy()
    return result


class PoetryDataset(Dataset):
    """
    Dataset class
    """
    def __init__(self, data: pd.DataFrame):
        """
        Init method
        :param data: poetry data frame
        """
        super(PoetryDataset, self).__init__()
        self.data: pd.DataFrame = data

    def __len__(self) -> int:
        return len(self.data)

    def __getitem__(self, index: int) -> Dict[str, Tensor]:
        """
        Get data instance
        :param index:
        :return: input ids, attention mask, labels
        """
        row: pd.Series = self.data.iloc[index]
        prompt = generate_prompt(row=row)
        output: BatchEncoding = tokenize_text(text=prompt)
        return {
            "input_ids": torch.tensor(output["input_ids"], dtype=torch.long),
            "attention_mask": torch.tensor(output["attention_mask"], dtype=torch.float),
            "labels": torch.tensor(output["labels"], dtype=torch.long)
        }


df = pd.read_csv(config["data_file"], dtype=str)
df.dropna(inplace=True, subset=["content"])
df.fillna(inplace=True, value="")
dataset = PoetryDataset(data=df)


model = AutoModelForCausalLM.from_pretrained(
    pretrained_model_name_or_path=config["pretrained_model"],
    load_in_8bit=True, torch_dtype=torch.float16, device_map="auto"
)
model = prepare_model_for_int8_training(model)
peft_config = LoraConfig(
    r=config["lora_r"], lora_alpha=config["lora_alpha"],
    lora_dropout=config["lora_dropout"],
    target_modules=config["lora_target_modules"],
    bias="none", task_type=TaskType.CAUSAL_LM
)
model = get_peft_model(model=model, peft_config=peft_config)


resume_from_checkpoint: Union[str, bool] = False
if config["resume_from_checkpoint"] is not None:
    checkpoint_name: str = os.path.join(
        config["resume_from_checkpoint"], "pytorch_model.bin"
    )    # full checkpoint from huggingface Trainer
    resume_from_checkpoint = config["resume_from_checkpoint"]
    if not os.path.exists(checkpoint_name):
        checkpoint_name: str = os.path.join(
            config["resume_from_checkpoint"], "adapter_model.bin"
        )    # only adapter weight, from model.save_pretrained()
        resume_from_checkpoint = False

    if os.path.exists(checkpoint_name):
        print(f"Restarting from {checkpoint_name}")
        adapters_weights = torch.load(checkpoint_name)
        set_peft_model_state_dict(
            model=model, peft_model_state_dict=adapters_weights
        )
    else:
        print(f"Checkpoint {checkpoint_name} not found")
        resume_from_checkpoint = False


model.print_trainable_parameters()
model.config.use_cache = False


trainer = Trainer(
    model=model,
    train_dataset=dataset,
    eval_dataset=None,
    args=TrainingArguments(
        per_device_train_batch_size=config["batch_size"],
        gradient_accumulation_steps=config["gradient_accumulation_steps"],
        num_train_epochs=config["num_epochs"],
        warmup_steps=100,
        learning_rate=config["learning_rate"],
        fp16=True,
        logging_steps=10,
        optim="adamw_torch",
        evaluation_strategy="no", eval_steps=None,
        save_strategy="steps", save_steps=200,
        output_dir=config["output_dir"],
        save_total_limit=3,
        load_best_model_at_end=False,
        ddp_find_unused_parameters=None,
        group_by_length=False,
        report_to=["tensorboard"]
    ),
    data_collator=DataCollatorForSeq2Seq(
        tokenizer=tokenizer, pad_to_multiple_of=8,
        return_tensors="pt", padding=True
    ),
)
trainer.train(resume_from_checkpoint=resume_from_checkpoint)


model.save_pretrained(config["output_dir"])
