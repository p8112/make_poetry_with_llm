import json
import streamlit as st
from typing import List, Dict
import yaml
from transformers import (
    AutoTokenizer, PreTrainedTokenizer,
    AutoModelForCausalLM
)
from peft import PeftModel
import torch
import torch.nn as nn


st.set_page_config(
    page_title="Make poetry demo", layout="centered"
)


st.title("Demo: make poetry with language model")
st.markdown("---")


with st.sidebar:
    st.subheader("Config for generation process")
    st.markdown("---")

    max_length: int = st.slider(
        label="Max length", min_value=1, max_value=512, value=150
    )
    min_length: int = st.slider(
        label="Min length", min_value=1, max_value=512, value=10
    )
    num_beams: int = st.slider(
        label="Num beams", min_value=1, max_value=100, value=4
    )
    temperature: float = st.slider(
        label="Temperature", min_value=0.0, max_value=1.0, value=0.5
    )
    top_k: int = st.slider(
        label="Top k", min_value=1, max_value=100, value=20
    )
    repetition_penalty: float = st.slider(
        label="Repetition penalty", min_value=1.0, max_value=10.0, value=1.2
    )
    no_repeat_ngram_size: int = st.slider(
        label="No repeat ngram size", min_value=1, max_value=10, value=4
    )
    num_return_sequences: int = st.slider(
        label="Num return sequences", min_value=1, max_value=5, value=1
    )
    left_column, right_column = st.columns(2)
    do_sample: bool = left_column.checkbox(
        label="Do sample", value=True
    )
    early_stopping: bool = right_column.checkbox(
        label="Early stopping", value=False
    )


@st.cache_resource
def load_config() -> Dict:
    """
    Load config
    :return:
    """
    with open("config.yaml", mode="r") as file_obj:
        result: Dict = yaml.safe_load(file_obj)
    return result


config: Dict = load_config()


@st.cache_resource
def load_genres() -> List[str]:
    """
    Load list of genres
    :return: list of genres
    """
    with open(config["genres_file"], mode="r") as file_obj:
        result: List[str] = json.load(file_obj)
    return result


left_column, right_column = st.columns(2)
left_column.subheader("Please choose a genre:")
selected_genre: str = left_column.selectbox(
    label="genre", options=load_genres(), label_visibility='hidden'
)
right_column.subheader("Please enter title:")
selected_title: str = right_column.text_input(
    label="title", label_visibility="hidden"
)
st.markdown("---")


def generate_prompt(genre: str, title: str) -> str:
    """
    Generate prompt for language model
    :param genre: genre
    :param title: title
    :return: prompt
    """
    if genre == "câu đối":
        result: str = f"Một {genre}"
    elif genre.startswith("thơ"):
        result: str = f"Một bài {genre}"
    else:
        result: str = f"Một bài thơ {genre}"

    if len(title) > 0:
        result += f" có tựa đề '{title}'"

    result += f"\n\n"

    return result


@st.cache_resource
def load_tokenizer() -> PreTrainedTokenizer:
    """
    Load tokenizer
    :return:
    """
    result: PreTrainedTokenizer = AutoTokenizer.from_pretrained(
        pretrained_model_name_or_path=config["pretrained_model"]
    )
    result.pad_token_id = 0
    result.padding_side = "left"
    return result


@st.cache_resource
def load_model() -> nn.Module:
    """
    Load model
    :return:
    """
    result = AutoModelForCausalLM.from_pretrained(
        pretrained_model_name_or_path=config["pretrained_model"],
        load_in_8bit=True, torch_dtype=torch.float16,
        device_map="auto"
    )
    result = PeftModel.from_pretrained(
        model=result, model_id=config["output_dir"],
        torch_dtype=torch.float16, device_map="auto"
    )
    result.eval()

    return result


if len(selected_title) > 0:
    with st.spinner(text="Generate text..."):
        tokenizer = load_tokenizer()
        model = load_model()

        prompt = generate_prompt(genre=selected_genre, title=selected_title)
        input_ids = tokenizer.encode(prompt, return_tensors='pt')
        outputs = model.model.generate(
            input_ids.to(model.device),
            eos_token_id=tokenizer.eos_token_id, pad_token_id=tokenizer.pad_token_id,
            do_sample=do_sample, max_length=max_length, min_length=min_length,
            top_k=top_k, num_beams=num_beams, early_stopping=early_stopping,
            no_repeat_ngram_size=no_repeat_ngram_size, num_return_sequences=num_return_sequences
        )

        for output in outputs:
            tmp: str = tokenizer.decode(
                output.tolist(), skip_special_tokens=True
            )
            tmp = tmp.replace("\n", "  \n ")
            st.write(tmp)
            st.markdown("---")
